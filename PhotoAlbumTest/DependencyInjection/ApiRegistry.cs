﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhotoAlbumTest.Services.DepencyInjection;
using StructureMap;

namespace PhotoAlbumTest.Api.DependencyInjection
{
    public class ApiRegistry : Registry
    {
        public ApiRegistry()
        {
            IncludeRegistry<ServicesRegistry>();

            Scan(x =>
            {
                //x.TheCallingAssembly();
                x.AssemblyContainingType<ApiRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
