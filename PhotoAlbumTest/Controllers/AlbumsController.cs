﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhotoAlbumTest.ServiceContract.ResponseModels;
using PhotoAlbumTest.Services;

namespace PhotoAlbumTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlbumsController : ControllerBase
    {
        private readonly IAlbumsService _albumsService;

        public AlbumsController(IAlbumsService albumsService)
        {
            _albumsService = albumsService;
        }

        /// <summary>
        /// Gets all Albums
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AlbumResponseModel>>> Get()
        {
            var albums = await _albumsService.GetAlbums();

            if (albums == null)
                return NotFound();

            return Ok(albums);
        }
    }
}
