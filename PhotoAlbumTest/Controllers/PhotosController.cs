﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhotoAlbumTest.ServiceContract.ResponseModels;
using PhotoAlbumTest.Services;

namespace PhotoAlbumTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private readonly IPhotosService _photosService;

        public PhotosController(IPhotosService photosService)
        {
            _photosService = photosService;
        }

        /// <summary>
        /// Gets all Photos
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PhotoResponseModel>>> Get()
        {
            var photos = await _photosService.GetPhotos();

            if (photos == null)
                return NotFound();

            return Ok(photos);
        }
    }
}
