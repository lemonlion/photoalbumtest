﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhotoAlbumTest.ServiceContract.ResponseModels;
using PhotoAlbumTest.Services;

namespace PhotoAlbumTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IPhotosService _photosService;
        private readonly IAlbumsService _albumsService;

        public UsersController(IPhotosService photosService, IAlbumsService albumsService)
        {
            _photosService = photosService;
            _albumsService = albumsService;
        }

        /// <summary>
        /// Gets all Photos belonging to specified User
        /// </summary>
        [HttpGet("{id}/photos")]
        public async Task<ActionResult<IEnumerable<PhotoResponseModel>>> GetPhotos(int id)
        {
            var photos = await _photosService.GetPhotos(id);

            if (photos == null)
                return NotFound();

            return Ok(photos);
        }

        /// <summary>
        /// Gets all Albums belonging to specified User
        /// </summary>
        [HttpGet("{id}/albums")]
        public async Task<ActionResult<IEnumerable<AlbumResponseModel>>> GetAlbums(int id)
        {
            var albums = await _albumsService.GetAlbums(id);

            if (albums == null)
                return NotFound();

            return Ok(albums);
        }
    }
}
