﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Data;
using Newtonsoft.Json;
using PhotoAlbumTest.Domain;
using PhotoAlbumTest.ServiceContract.ResponseModels;

namespace PhotoAlbumTest.Services
{
    public class PhotosService : IPhotosService
    {
        private readonly IPhotoAlbumDataRetriever _dataRetriever;

        public PhotosService(IPhotoAlbumDataRetriever dataRetriever)
        {
            _dataRetriever = dataRetriever;
        }

        public async Task<IEnumerable<PhotoResponseModel>> GetPhotos(int? userId = null)
        {
            var photos = await _dataRetriever.GetPhotos();
            var albums = await _dataRetriever.GetAlbums();

            var photosResponseModels = photos.Select(x => new PhotoResponseModel
            {
                Id = x.Id,
                Title = x.Title,
                Album = albums.Where(y => y.Id == x.AlbumId).Select(y => new AlbumResponseModel
                {
                    Id = y.Id,
                    Title = y.Title,
                    UserId = y.UserId
                }).Single()
            });

            if(userId != null)
                photosResponseModels = photosResponseModels.Where(x => x.Album.UserId == userId).ToList();

            return photosResponseModels;
        }
    }
}
