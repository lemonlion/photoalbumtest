﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PhotoAlbumTest.Domain;
using PhotoAlbumTest.ServiceContract.ResponseModels;

namespace PhotoAlbumTest.Services
{
    public class AlbumsService : IAlbumsService
    {
        private readonly IPhotoAlbumDataRetriever _dataRetriever;

        public AlbumsService(IPhotoAlbumDataRetriever dataRetriever)
        {
            _dataRetriever = dataRetriever;
        }

        public async Task<IEnumerable<AlbumResponseModel>> GetAlbums(int? userId = null)
        {
            var albums = await _dataRetriever.GetAlbums();
            albums = userId == null ? albums : albums.Where(x => x.UserId == userId).ToList();

            var photos = await _dataRetriever.GetPhotos();
            
            var albumsResponseModels = albums.Select(x => new AlbumResponseModel
            {
                Id = x.Id,
                Title = x.Title,
                UserId = x.UserId,
                Photos = photos.Where(y => y.AlbumId == x.Id).Select(y => new PhotoResponseModel
                {
                    Id = y.Id,
                    ThumbnailUrl = y.ThumbnailUrl,
                    Title = y.Title,
                    Url = y.Url
                })
            });

            return albumsResponseModels;
        }
    }
}
