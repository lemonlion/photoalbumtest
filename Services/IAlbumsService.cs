﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PhotoAlbumTest.ServiceContract.ResponseModels;

namespace PhotoAlbumTest.Services
{
    public interface IAlbumsService
    {
        Task<IEnumerable<AlbumResponseModel>> GetAlbums(int? userId = null);
    }
}