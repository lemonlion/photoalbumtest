﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PhotoAlbumTest.ServiceContract.ResponseModels;

namespace PhotoAlbumTest.Services
{
    public interface IPhotosService
    {
        Task<IEnumerable<PhotoResponseModel>> GetPhotos(int? userId = null);
    }
}