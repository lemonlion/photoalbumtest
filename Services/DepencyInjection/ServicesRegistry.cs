﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.DependencyInjection;
using StructureMap;

namespace PhotoAlbumTest.Services.DepencyInjection
{
    public class ServicesRegistry : Registry
    {
        public ServicesRegistry()
        {
            IncludeRegistry<DataRegistry>();

            Scan(x =>
            {
                x.AssemblyContainingType<ServicesRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
