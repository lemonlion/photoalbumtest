using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Moq;
using PhotoAlbumTest.Domain.Models;
using Shouldly;
using Xunit;

namespace PhotoAlbumTest.Services.Test.Unit
{
    public class PhotosServiceTests
    {
        private readonly List<Photo> photosResponse = new List<Photo>()
        {
            new Photo { Id = 1, Title = "Photo1", AlbumId = 1 },
            new Photo { Id = 2, Title = "Photo2", AlbumId = 2 },
            new Photo { Id = 3, Title = "Photo3", AlbumId = 3 },
        };
        private readonly List<Album> albumsResponse = new List<Album>()
        {
            new Album { Id = 1, Title = "Album1", UserId  = 1 },
            new Album { Id = 2, Title = "Album2", UserId  = 2 },
            new Album { Id = 3, Title = "Album3", UserId  = 2 },
        };

        private readonly Mock<IPhotoAlbumDataRetriever> _photoAlbumDataRetrieverMock = new Mock<IPhotoAlbumDataRetriever>();

        public PhotosServiceTests()
        {
            // Arrange
            _photoAlbumDataRetrieverMock.Setup(x => x.GetPhotos()).ReturnsAsync(photosResponse);
            _photoAlbumDataRetrieverMock.Setup(x => x.GetAlbums()).ReturnsAsync(albumsResponse);
        }

        [Fact]
        public async Task When_No_User_Id_Supplied_Then_Return_All_Results()
        {
            // Arrange
            var serviceUnderTest = new PhotosService(_photoAlbumDataRetrieverMock.Object);

            // Act
            var result = await serviceUnderTest.GetPhotos();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count().ShouldBe(3);
        }

        [Fact]
        public async Task When_User_Id_Supplied_Then_Return_Results_For_That_User_Only()
        {
            // Arrange
            var serviceUnderTest = new PhotosService(_photoAlbumDataRetrieverMock.Object);

            // Act
            var userId = 2;
            var result = await serviceUnderTest.GetPhotos(userId);

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count().ShouldBe(2);
            result.All(x => x.Album.UserId == userId).ShouldBeTrue();
        }
    }
}
