using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Moq;
using Moq.Protected;
using PhotoAlbumTest.Domain.Settings;
using Shouldly;
using Xunit;

namespace PhotoAlbumTest.Data.Tests.Unit
{
    public class PhotoAlbumDataRetrieverTests
    {
        private EndpointSettings _endpointSettings = new EndpointSettings { AlbumsEndpointUrl = "http://example.com/albums", PhotosEndpointUrl = "http://example.com/photos" };
        
        private PhotoAlbumDataRetriever _serviceUnderTest;
        
        [Fact]
        public async Task When_GetPhotos_Is_Called_It_Should_Return_Photos_If_Source_Has_Photos()
        {
            // Arrange
            var emptyResponse = "[]";
            var httpClientFactory = GetClientFactoryWithSendAsyncResponse(_endpointSettings.PhotosEndpointUrl, emptyResponse);
            _serviceUnderTest = new PhotoAlbumDataRetriever(httpClientFactory, _endpointSettings);

            // Act
            var photos = await _serviceUnderTest.GetPhotos();

            // Assert
            photos.ShouldNotBeNull();
            photos.ShouldBeEmpty();
        }

        [Fact]
        public async Task When_GetPhotos_Is_Called_It_Should_Return_Empty_If_Source_Is_Empty()
        {
            // Arrange
            var threePhotosResponse = @"[
            {
                ""albumId"": 1,
                ""id"": 1,
                ""title"": ""accusamus beatae ad facilis cum similique qui sunt"",
                ""url"": ""https://via.placeholder.com/600/92c952"",
                ""thumbnailUrl"": ""https://via.placeholder.com/150/92c952""
            },
            {
                ""albumId"": 1,
                ""id"": 2,
                ""title"": ""reprehenderit est deserunt velit ipsam"",
                ""url"": ""https://via.placeholder.com/600/771796"",
                ""thumbnailUrl"": ""https://via.placeholder.com/150/771796""
            },
            {
                ""albumId"": 1,
                ""id"": 3,
                ""title"": ""officia porro iure quia iusto qui ipsa ut modi"",
                ""url"": ""https://via.placeholder.com/600/24f355"",
                ""thumbnailUrl"": ""https://via.placeholder.com/150/24f355""
            }]";
            var httpClientFactory = GetClientFactoryWithSendAsyncResponse(_endpointSettings.PhotosEndpointUrl, threePhotosResponse);
            _serviceUnderTest = new PhotoAlbumDataRetriever(httpClientFactory, _endpointSettings);

            // Act
            var photos = await _serviceUnderTest.GetPhotos();

            // Assert
            photos.ShouldNotBeNull();
            photos.Count().ShouldBe(3);
        }

        [Fact]
        public async Task When_GetAlbums_Is_Called_It_Should_Return_Albums_If_Source_Has_Albums()
        {
            // Arrange
            var emptyResponse = "[]";
            var httpClientFactory = GetClientFactoryWithSendAsyncResponse(_endpointSettings.AlbumsEndpointUrl, emptyResponse);
            _serviceUnderTest = new PhotoAlbumDataRetriever(httpClientFactory, _endpointSettings);

            // Act
            var albums = await _serviceUnderTest.GetAlbums();

            // Assert
            albums.ShouldNotBeNull();
            albums.ShouldBeEmpty();
        }

        [Fact]
        public async Task When_GetAlbums_Is_Called_It_Should_Return_Empty_If_Source_Is_Empty()
        {
            // Arrange
            var threeAlbumsResponse = @"[
            {
                ""userId"": 1,
                ""id"": 1,
                ""title"": ""quidem molestiae enim""
            },
            {
                ""userId"": 1,
                ""id"": 2,
                ""title"": ""sunt qui excepturi placeat culpa""
            },
            {
                ""userId"": 1,
                ""id"": 3,
                ""title"": ""omnis laborum odio""
            }]";
            var httpClientFactory = GetClientFactoryWithSendAsyncResponse(_endpointSettings.AlbumsEndpointUrl, threeAlbumsResponse);
            _serviceUnderTest = new PhotoAlbumDataRetriever(httpClientFactory, _endpointSettings);

            // Act
            var albums = await _serviceUnderTest.GetAlbums();

            // Assert
            albums.ShouldNotBeNull();
            albums.Count().ShouldBe(3);
        }


        /// <summary>
        /// Allows you to Mock HttpClient/HttpClientFactory internals essentially
        /// </summary>
        private IHttpClientFactory GetClientFactoryWithSendAsyncResponse(string requestUri, string response)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(x => x.RequestUri.ToString() == requestUri), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(response),
                })
                .Verifiable();

            return new TestHttpClientFactory(handlerMock.Object);
        }
    }

    internal class TestHttpClientFactory : IHttpClientFactory
    {
        private HttpMessageHandler _messageHandler;

        public TestHttpClientFactory(HttpMessageHandler messageHandler)
        {
            _messageHandler = messageHandler;
        }

        public HttpClient CreateClient(string name) => new HttpClient(_messageHandler);
    }
}
