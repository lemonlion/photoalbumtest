﻿using System.Collections.Generic;

namespace PhotoAlbumTest.ServiceContract.ResponseModels
{
    public class AlbumResponseModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }

        public IEnumerable<PhotoResponseModel> Photos { get; set; }
    }
}
