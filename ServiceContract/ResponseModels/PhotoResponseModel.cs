﻿namespace PhotoAlbumTest.ServiceContract.ResponseModels
{
    public class PhotoResponseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }

        public AlbumResponseModel Album { get; set; }
    }
}
