﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoAlbumTest.Domain.Settings
{
    public class EndpointSettings
    {
        public string PhotosEndpointUrl { get; set; }
        public string AlbumsEndpointUrl { get; set; }
    }
}
