﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PhotoAlbumTest.Domain;
using PhotoAlbumTest.Domain.Models;
using PhotoAlbumTest.Domain.Settings;

namespace Data
{
    public class PhotoAlbumDataRetriever : IPhotoAlbumDataRetriever
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly EndpointSettings _endpointSettings;

        public PhotoAlbumDataRetriever(IHttpClientFactory httpClientFactory, EndpointSettings endpointSettings)
        {
            _httpClientFactory = httpClientFactory;
            _endpointSettings = endpointSettings;
        }

        // Would be good to have caching here
        public async Task<IEnumerable<Photo>> GetPhotos()
        {
            var httpClient = _httpClientFactory.CreateClient();
            var photosRawString = await httpClient.GetStringAsync(_endpointSettings.PhotosEndpointUrl);
            var photos = JsonConvert.DeserializeObject<List<Photo>>(photosRawString);
            return photos;
        }

        // Would be good to have caching here
        public async Task<IEnumerable<Album>> GetAlbums()
        {
            var httpClient = _httpClientFactory.CreateClient();
            var albumsRawString = await httpClient.GetStringAsync(_endpointSettings.AlbumsEndpointUrl);
            var albums = JsonConvert.DeserializeObject<List<Album>>(albumsRawString);
            return albums;
        }
    }
}
