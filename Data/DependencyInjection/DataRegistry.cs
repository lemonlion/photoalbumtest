﻿using System;
using System.Collections.Generic;
using System.Text;
using StructureMap;

namespace Data.DependencyInjection
{
    public class DataRegistry : Registry
    {
        public DataRegistry()
        {
            Scan(x =>
            {
                x.AssemblyContainingType<DataRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
