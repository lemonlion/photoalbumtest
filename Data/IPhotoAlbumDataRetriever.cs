﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PhotoAlbumTest.Domain;
using PhotoAlbumTest.Domain.Models;

namespace Data
{
    public interface IPhotoAlbumDataRetriever
    {
        Task<IEnumerable<Album>> GetAlbums();
        Task<IEnumerable<Photo>> GetPhotos();
    }
}